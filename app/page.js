
import Image from 'next/image'
import BannerOne from "../components/BannerOne";
import AboutAreaOne from "../components/AboutAreaOne";
import ServiceAreaOne from "../components/ServiceAreaOne";
import FaqAreaOne from "../components/FaqAreaOne";
import CaseStudyAreaOne from "../components/CaseStudyAreaOne";
import CounterAreaOne from "../components/CounterAreaOne";
import ContactAreaOne from "../components/ContactAreaOne";
import WorkProcessOne from "../components/WorkProcessOne";
import PricingAreaOne from "../components/PricingAreaOne";
import BlogAreaOne from "../components/BlogAreaOne";
export default async function Home() {
  return (
    <>

      {/* Banner One */}
      <BannerOne />

      {/* About Area One */}
      <AboutAreaOne />

      {/* Service Area One */}
      <ServiceAreaOne />

      {/* FAQ Area One */}
      <FaqAreaOne />

      
      {/* Case Study Area One */}
      <CaseStudyAreaOne />

      {/* Counter Area One */}
      <CounterAreaOne />

      {/* Contact Area One */}
      <ContactAreaOne />      
      
      {/* Work Process One */}
      <WorkProcessOne />

      {/* Pricing Area One */}
      <PricingAreaOne />


      {/* Blog Area One */}
      <BlogAreaOne />
      



    </>
  )
}
