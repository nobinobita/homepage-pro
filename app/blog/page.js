import BlogGroup from "@/components/BlogGroup";
import Breadcrumb from "@/components/Breadcrumb";

const Blog = () => {
  return (
    <>

      {/* Navigation Bar */}
      <Breadcrumb title={"Blog"} />

      {/* Blog Group */}
      <BlogGroup />

    </>
  );
};

export default Blog;
