import BlogDetailsArea from "@/components/BlogDetailsArea";
import Breadcrumb from "@/components/Breadcrumb";
import FooterOne from "@/components/FooterOne";
import NavBar from "@/components/NavBar";

const BlogDetails = () => {
  return (
    <>

      {/* Navigation Bar */}
      <Breadcrumb title={"Blog Details"} />

      {/* Blog Details Area */}
      <BlogDetailsArea />
      
    </>
  );
};

export default BlogDetails;
