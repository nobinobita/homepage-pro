"use client";
import Breadcrumb from "@/components/Breadcrumb";
import ContactMain from "@/components/ContactMain";
const Contact = () => {
  return (
    <>
      {/* Navigation Bar */}
      <Breadcrumb title={"Contact"} />

      {/* Contact Main */}
      <ContactMain />
    </>
  );
};

export default Contact;
