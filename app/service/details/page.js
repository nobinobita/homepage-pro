import Breadcrumb from "@/components/Breadcrumb";
import ServiceDetailsArea from "@/components/ServiceDetailsArea";

const Details = () => {
  return (
    <>
      {/* Navigation Bar */}
      <Breadcrumb title={"Service Details"} />

      {/* Service Details Area */}
      <ServiceDetailsArea />

    </>
  );
};

export default Details;
