import Breadcrumb from "@/components/Breadcrumb";
import TeamDetailsGroup from "@/components/TeamDetailsGroup";

const Details = () => {
  return (
    <>

      {/* Breadcrumb */}
      <Breadcrumb title={"Team Details"} />

      {/* Team Details Group */}
      <TeamDetailsGroup />
    </>
  );
};

export default Details;
