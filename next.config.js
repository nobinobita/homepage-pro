/** @type {import('next').NextConfig} */
const config = require("./config.js.default");

const nextConfig = {
  env: {
    BASE_URL: config.BASE_URL,
    API: config.API,
  },
};
module.exports = nextConfig
